package com.mopal.model schema data;

enum TipoCamino{
    INGRESANTE;
    CAMINANTE;
}

enum EstadoCivil{
    SOLTERO;
    CASADO;
    SEPARADO;
    VIUDO;
}

enum TipoEvento{
    JORNADA_PENTECOSTES;
    RETIRO_DE_PASCUA;
    ASAMBLEA;
}